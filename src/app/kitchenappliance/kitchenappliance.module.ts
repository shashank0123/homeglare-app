import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { KitchenappliancePage } from './kitchenappliance.page';

const routes: Routes = [
  {
    path: '',
    component: KitchenappliancePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [KitchenappliancePage]
})
export class KitchenappliancePageModule {}
