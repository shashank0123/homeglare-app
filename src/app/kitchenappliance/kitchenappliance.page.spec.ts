import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitchenappliancePage } from './kitchenappliance.page';

describe('KitchenappliancePage', () => {
  let component: KitchenappliancePage;
  let fixture: ComponentFixture<KitchenappliancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitchenappliancePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitchenappliancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
