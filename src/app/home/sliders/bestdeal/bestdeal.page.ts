import { Component, OnInit } from '@angular/core';
import  { Router  } from '@angular/router';

import Swiper from 'swiper';
@Component({
  selector: 'app-bestdeal',
  templateUrl: './bestdeal.page.html',
  styleUrls: ['./bestdeal.page.scss'],
})
export class BestdealPage implements OnInit {

// =========== slider js ==============
   sliderConfig = {
    zoom: false,
    slidesPerView: 1.4,
    spaceBetween: 10,
    centeredSlides: false
  };
// =========== end slider js ==============
  constructor(private router:Router) { }

 cartDetails( ){
this.router.navigate(['productdetails']);

  }

  ngOnInit() {

 

  }

}
