import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestdealPage } from './bestdeal.page';

describe('BestdealPage', () => {
  let component: BestdealPage;
  let fixture: ComponentFixture<BestdealPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestdealPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestdealPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
