import { Component, OnInit } from '@angular/core';
import  { Router  } from '@angular/router';

import Swiper from 'swiper';
@Component({
  selector: 'app-trending',
  templateUrl: './trending.page.html',
  styleUrls: ['./trending.page.scss'],
})
export class TrendingPage implements OnInit {


  // =========== slider js ==============
   sliderConfig2 = {
    zoom: false,
    slidesPerView: 2.3,
    spaceBetween: 3,
    centeredSlides: false
  };
// =========== end slider js ==============

  constructor( private router:Router ) {


   }



  cartDetails(){
this.router.navigate(['productdetails']);

  }

  ngOnInit() {
  	


  }

}
