import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import  { BestdealPage } from './sliders/bestdeal/bestdeal.page';
import  { TrendingPage } from './sliders/trending/trending.page'


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: HomePage }])
  ],
  declarations: [HomePage,BestdealPage,TrendingPage],
exports:[BestdealPage,TrendingPage]

})
export class HomePageModule {}
