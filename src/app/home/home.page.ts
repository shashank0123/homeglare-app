import { Component, OnInit } from '@angular/core';
import Swiper from 'swiper';
import  { Router  } from '@angular/router';
import  { BestdealPage } from './sliders/bestdeal/bestdeal.page';
import  { TrendingPage } from './sliders/trending/trending.page'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {

// =========== slider js ==============
	slideOpts = {
    autoplay: {
      delay: 2000,

    },
    zoom: false,
    effect: 'flip',
    spaceBetween: 10,
     slidesPerView: 1.3,
  };

// =========== end slider js ==============
 constructor( private router:Router ) {


   }
allview(){
	this.router.navigate(['kitchenappliance']);
}


ngOnInit(){

}


	
}


