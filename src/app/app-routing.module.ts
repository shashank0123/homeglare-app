import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'productdetails', loadChildren: './productdetails/productdetails.module#ProductdetailsPageModule' },
  { path: 'trending', loadChildren: './home/sliders/trending/trending.module#TrendingPageModule' },
  { path: 'bestdeal', loadChildren: './home/sliders/bestdeal/bestdeal.module#BestdealPageModule' },
  { path: 'kitchenappliance', loadChildren: './kitchenappliance/kitchenappliance.module#KitchenappliancePageModule' },
  { path: 'wishlist', loadChildren: './wishlist/wishlist.module#WishlistPageModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' },
  { path: 'check-out', loadChildren: './check-out/check-out.module#CheckOutPageModule' },
  { path: 'search', loadChildren: './search/search.module#SearchPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'address', loadChildren: './address/address.module#AddressPageModule' }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
