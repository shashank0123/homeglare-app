import { MenuController } from '@ionic/angular';
import { Component, OnInit, OnDestroy } from '@angular/core';
import Swiper from 'swiper';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

  constructor(
    private menuCtrl: MenuController
  ) { }

  ngOnInit() {
    this.menuCtrl.enable(false);



    //  var swiper = new Swiper('.swiper-container', {
    //   slidesPerView: 2,
    //   spaceBetween: 0,
    //   slidesPerGroup: 2,
    //   loop: true,
    //   loopFillGroupWithBlank: true,
     
    //   navigation: {
    //     nextEl: '.swiper-button-next',
    //     prevEl: '.swiper-button-prev',
    //   },
    // });


     
  }

  ngOnDestroy() {
    this.menuCtrl.enable(true);
  }

}
